## rPPG2ABP 

Obtención de señal de presión sanguinea (señal continua, estimación de DBP y SBP) a partir de rPPG.

Paper: https://arxiv.org/pdf/2005.01669.pdf 

Repo original: https://github.com/nibtehaz/PPG2ABP 


1. Instalación de ambiente:

`conda create --name ppg2abp python=3.5`

`conda activate ppg2abp`

`pip install -r requirements.txt `

`conda install cudatoolkit=9.0`

`conda install cudnn=7.1.2`

2. Predicción de BP a partir de rPPG:

`cd codes`

`python predict_rppg.py` -> utiliza una señal rPPG de 1024 muestras, 8.192 segundos, 125Hz. Dentro del programa pide la ruta de la data.

Rutas disponibles:
1. data/ppg_prediction.p
2. data/dataset-yarppg/01.p -> de 01 a 09


Para transformar señales al formato de entrada de la red, revisar el script en codes/preprocess_yarppg.py
