import scipy.io
from scipy.signal import butter
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import os

#dir = os.getcwd()
path = 'data/yarppg/'

nombre = 'outputs.csv'
nombre_destino = 'ppg_prediction.p'

#Leer data y obtener 8.192 segundos
data = pd.read_csv(path + nombre) 
data_rango = data[(data.ts >= 10) & (data.ts <= 18.192)]
t1 = data_rango.ts.values - 10
rppg = data_rango.p0.values

#upsample 30 -> 125 Hz
t2 = np.linspace(0, 8.191, 1024)
upsampled_pulse = np.interp(t2, t1, rppg)

#filtro pasabanda
fs = 125
[b_pulse, a_pulse] = butter(1, [0.75 / fs * 2, 2.5 / fs * 2], btype='bandpass')
pulse_pred = scipy.signal.filtfilt(b_pulse, a_pulse, np.double(upsampled_pulse))/0.8 +0.5

#save
pickle.dump(upsampled_pulse,open(path + nombre_destino,'wb'))
