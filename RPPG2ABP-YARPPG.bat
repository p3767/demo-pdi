::CD to rppg2abp folder from current dir
cd %~dp0rppg2abp\codes
::Transform yarppg .csv to .p file
python single_preprocess_yarppg.py
::Activate enviroment
CALL conda.bat activate rppg2abp
::Python Script for BP Signal
python predict_rppg.py --method yarppg
::Deactivate enviroment
CALL conda.bat deactivate