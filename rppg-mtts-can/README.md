## rPPG con MTTS-CAN

Obtención de señal rPPG a partir de un video del rostro.

Paper: https://papers.nips.cc/paper/2020/file/e1228be46de6a0234ac22ded31417bc7-Paper.pdf

Git original: https://github.com/xliucs/MTTS-CAN 

Instrucciones de ambiente:

`conda create -n mtts-can tensorflow-gpu cudatoolkit=10.1`

`conda activate mtts-can`

`pip install opencv-python scipy numpy matplotlib scikit-image`

Instrucciones para obtener señal rPPG:

1. Grabar video del rostro:

`python code/grabar.py`  -> se grabarán 8.192 segundos a 30 fps (246 frames), y se guardarán en output.mp4

2. Predecir rPPG:

`python code/predict_vitals.py --video_path output.mp4`   -> se obtiene la señal, se normaliza y se realiza upsample de 30 a 125 Hz. La salida queda en ppg_prediction.p
