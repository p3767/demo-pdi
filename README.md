# Demo PDI
## Descripción
Demo del proyecto a desarrollar. Consiste en una integración de una serie de proyectos relacionados con la detección de señales rPPG y el calculo de presión sanguínea

### Proyectos relacionados:  
-Yarppg: https://gitlab.com/p3767/yarppg (Obtención señal rPGG con método matemático)  
-RPPG-MTTS-CAN: https://gitlab.com/p3767/rppg-mtts-can (Obtención señal rPGG con red neuronal)  
-rPPG2ABP: https://gitlab.com/p3767/rppg2abp (Cálculo de BP con red neuronal)  

## Instalación  
Revisar los requisitos en la sección de "proyectos relacionados". Para más detalle revisar el Manual de Instalación.  
El programa es autocontenido y detecta automática el directorio en el que se encuentra, por lo que no necesita de una ubicación especifica para su uso.  
Diseñado para sistema operativo Windows.  

## Uso  


## Estado del Proyecto  
