import sys
import os
import subprocess
import pathlib as Path
from PyQt5 import QtGui

from PyQt5.QtGui import QPixmap

from subwindow import *

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class MainApp(QMainWindow):
    def __init__(self):
        super(MainApp, self).__init__()
        self.setWindowTitle('Blood Pressure Test')
        self.setWindowIcon(QtGui.QIcon('pulse-monitor.png'))
        self.setFixedWidth(450)
        self.setFixedHeight(300)

        self.generalLayout = QVBoxLayout()
        self._centralWidget = QWidget(self)
        self.setCentralWidget(self._centralWidget)
        self._centralWidget.setLayout(self.generalLayout)

        self._createButtons()

    def _createButtons(self):
        self.sub_window1 = SubWindow1()
        self.sub_window2 = SubWindow2()

        dlgLayout = QVBoxLayout()

        logoLabel = QLabel(self)
        pixmap = QPixmap('pulse-monitor.png')
        pixmap =  pixmap.scaled(200, 200)
        logoLabel.setPixmap(pixmap)
    
        button_1 = QPushButton(self)
        button_1.setText('YARPPG')
        button_1.setStyleSheet('font-size:40px')
        button_1.clicked.connect(self.sub_window1.show)
        

        button_2 = QPushButton(self)
        button_2.setText('MTTS-CAN')
        button_2.setStyleSheet('font-size:40px')
        button_2.clicked.connect(self.sub_window2.show)
        
        dlgLayout.addWidget(logoLabel, alignment=Qt.AlignCenter)
        dlgLayout.addWidget(button_1)
        dlgLayout.addWidget(button_2)
        
        self.generalLayout.addLayout(dlgLayout)


if __name__ == '__main__':
    app = QApplication([])
    window = MainApp()
    window.show()
    sys.exit(app.exec_())