::CD to mtts folder from current dir
cd %~dp0rppg-mtts-can
::Activate enviroment
CALL conda.bat activate mtts-can 
::Python scripts for rppg signal
python code/grabar.py 
python code/predict_vitals.py --video_path output.mp4
::Deactivate enviroment
CALL conda.bat deactivate