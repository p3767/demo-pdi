::CD to rppg2abp folder from current dir
cd %~dp0rppg2abp\codes
::Activate enviroment
CALL conda.bat activate rppg2abp
::Python Script for BP Signal
python predict_rppg.py --method mtts
::Deactivate enviroment
CALL conda.bat deactivate