import os
import subprocess
import sys

from PyQt5 import QtCore
from subwindow2 import *
from PyQt5 import QtGui, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class SubWindow1(QWidget):
    def __init__(self):
        super(SubWindow1, self).__init__()

        # Label
        self.label = QLabel(self)
        self.label.setAlignment(Qt.AlignCenter)

        self.setWindowTitle('Método Yarppg')
        self.setWindowIcon(QtGui.QIcon('pulse-monitor.png'))
        self.setFixedWidth(440)
        self.setFixedHeight(150)

        self.generalLayout = QVBoxLayout()
        self._createButtons()

    def _createButtons(self):
        self.sub_window1 = SubWindoww1()
        self.sub_window2 = SubWindoww2()

        dlgLayout = QVBoxLayout()

        button_1 = QPushButton(self)
        button_1.setText('YARPPG Toma de Video')
        button_1.setStyleSheet('font-size:40px')
        button_1.clicked.connect(self.runbutton1)
        
        button_2 = QPushButton(self)
        button_2.setText('Resultado PB')
        button_2.setStyleSheet('font-size:40px')
        button_2.clicked.connect(self.runbutton2)
        button_2.move(90,70)
        
        dlgLayout.addWidget(button_1)
        dlgLayout.addWidget(button_2)
                
        self.generalLayout.addLayout(dlgLayout)

    def runbutton1(self):
        file="YARPPGG.bat"
        subprocess.call(os.path.abspath(file))
    
    def runbutton2(self):
        file="RPPG2ABP-YARPPG.bat"
        subprocess.call(os.path.abspath(file))

class SubWindow2(QWidget):
    def __init__(self):
        super(SubWindow2, self).__init__()
        self.resize(400, 300)

        # Label
        self.label = QLabel(self)
        self.label.setGeometry(0, 0, 400, 300)
        self.label.setAlignment(Qt.AlignCenter)
        self.setWindowTitle('Método MTTS-CAN')
        self.setWindowIcon(QtGui.QIcon('pulse-monitor.png'))
        self.setFixedWidth(440)
        self.setFixedHeight(150)

        self.generalLayout = QVBoxLayout()
        self._createButtons()

    def _createButtons(self):
        self.sub_window1 = SubWindoww1()
        self.sub_window2 = SubWindoww2()

        dlgLayout = QVBoxLayout()

        button_1 = QPushButton(self)
        button_1.setText('MTTS Toma de Video')
        button_1.setStyleSheet('font-size:40px')
        button_1.clicked.connect(self.open_dialog)
        
        button_2 = QPushButton(self)
        button_2.setText('Resultado BP')
        button_2.setStyleSheet('font-size:40px')
        button_2.clicked.connect(self.runbutton2)
        button_2.move(90,70)

        dlgLayout.addWidget(button_1)
        dlgLayout.addWidget(button_2)
        
        self.generalLayout.addLayout(dlgLayout)

    def open_dialog(self):
        file="MTTS.bat"
        subprocess.call(os.path.abspath(file))

    def runbutton2(self):
        file="RPPG2ABP-MTTS.bat"
        subprocess.call(os.path.abspath(file))